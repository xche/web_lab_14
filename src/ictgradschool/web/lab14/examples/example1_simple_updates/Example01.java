package ictgradschool.web.lab14.examples.example1_simple_updates;

import ictgradschool.web.lab14.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Example01 {
    public static void main(String[] args) {

        // Load JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(Config.url, Config.getProperties())) {
            System.out.println("Connection successful");

            // Insert a row. Note that this is just and example and we should ALWAYS use parameterized queries & updates (see other examples)!
            try (Statement stmt = conn.createStatement()) {
                int rowsAffected = stmt.executeUpdate("INSERT INTO unidb_courses (dept, num, descrip, coord_no, rep_id) VALUES ('TEST', 123, 'A description', 666, 1713);");
                System.out.println(rowsAffected + " rows Inserted successfully");
            }

            // Delete a row. Note that this is just and example and we should ALWAYS use parameterized queries & updates (see other examples)!
            try (Statement stmt = conn.createStatement()) {
                int rowsAffected = stmt.executeUpdate("DELETE FROM unidb_courses WHERE dept = 'TEST' AND num = 123;");
                System.out.println(rowsAffected + " rows Deleted successfully");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
