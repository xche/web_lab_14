package ictgradschool.web.lab14;

import java.util.Properties;

/**
 * Allows the program to get properties such as username & password from a centralized location.
 */
public class Config {

    public static final String url = "jdbc:mysql://mysql1.sporadic.co.nz:3306/xche824";

    private static Properties properties;

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties(System.getProperties());
            properties.setProperty("user", "xche824");
            properties.setProperty("password", "xingjian-0528");
            properties.setProperty("useSSL", "true");
        }
        return properties;
    }
}
