package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.Keyboard;

import java.sql.*;

/**
 * Created by xingjianche on 10/05/2017.
 */
public class FilmDAO {
    public void searchByActor(String actorName){


        try (Connection conn = DriverManager.getConnection(Config.url, Config.getProperties())) {
            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT * FROM lab14_films,lab14_actors,lab14_involve WHERE lab14_actors.actorid=lab14_involve.actorid AND lab14_films.filmid=lab14_involve.filmid AND lab14_actors.name LIKE ?"
            )) {
                stmt.setString(1, "%" + actorName + "%");

                try (ResultSet rs = stmt.executeQuery()) {

                    if (!rs.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
                        return;
                    }

                    while (rs.next()) {

                        System.out.println(rs.getString("lab14_films.name") + " (" + rs.getString(rs.findColumn("lab14_involve.role"))+ ")");
                    }
                    System.out.println();
                }

            }
        } catch (SQLException e) {
            System.out.println("Failed to create the database connection.");
        }
    }
}
