package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.Keyboard;

import java.sql.*;

/**
 * Created by xingjianche on 10/05/2017.
 */

public class GenreDAO {
    public void searchByGenre(String genre) {



        try (Connection conn = DriverManager.getConnection(Config.url, Config.getProperties())) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lab14_films WHERE genre LIKE ?")) {
                stmt.setString(1, "%" + genre + "%");
                try (ResultSet rs = stmt.executeQuery()) {


                    if (!rs.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that genre\n");
                        return;
                    }

                    System.out.println("\nThe " + genre + " genre includes the following films:\n");
                    while (rs.next()) {
                        System.out.println(rs.getString(rs.findColumn("name")));
                    }
                    System.out.println();

                }
            }

        } catch (SQLException e) {
            System.out.println("Failed to create the database connection.");
        }
    }
}
