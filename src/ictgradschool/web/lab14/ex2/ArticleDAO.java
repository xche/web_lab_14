package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by xingjianche on 10/05/2017.
 */
public class ArticleDAO {

    public List<Article> allArticle(String title) {

        List<Article> articles = new ArrayList<>();

        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/xche824", Config.getProperties())) {
            System.out.println("Connection successful");


                try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM simpledao_articles WHERE title LIKE ?;")) {
                    stmt.setString(1, "%" + title + "%");
                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            Article a = new Article();
                            a.setArticleID(r.getInt(1));
                            a.setArticleTitle(r.getString(2));
                            a.setArticleBody(r.getString(3));
                            articles.add(a);
                        }
                    }
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return articles;
    }


}
