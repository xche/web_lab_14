package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Keyboard;

import java.util.List;

/**
 * Created by xingjianche on 10/05/2017.
 */
public class Article {
    private int articleID;
    private String articleTitle;
    private String articleBody;

    public void setArticleID(int id) {
        this.articleID = id;
    }

    public void setArticleTitle(String title) {
        this.articleTitle = title;
    }

    public void setArticleBody(String body) {
        this.articleBody = body;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getArticleTitle(){
        return articleTitle;
    }

    public String getArticleBody(){
        return articleBody;
    }


    public static void main(String[] args) {

        System.out.print("Please enter a title: ");
        String title = Keyboard.readInput();
        List<Article> articles = new ArticleDAO().allArticle(title);

        if (articles.isEmpty()) {
            System.out.println("No such article.");
        } else {
            for (Article article : articles) {
                System.out.println(article.getArticleBody());
            }
        }
    }
}
